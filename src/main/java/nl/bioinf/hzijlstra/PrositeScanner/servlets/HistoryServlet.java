package nl.bioinf.hzijlstra.PrositeScanner.servlets;

import nl.bioinf.hzijlstra.PrositeScanner.config.WebConfig;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Hans Zijlstra
 */
@WebServlet(name = "HistoryServlet", urlPatterns = {"/history"})
public class HistoryServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WebContext ctx = new WebContext(request,response,request.getServletContext(), request.getLocale());
        WebConfig.getTemplateEngine().process("browshistory", ctx, response.getWriter());

    }
}
