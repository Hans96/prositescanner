package nl.bioinf.hzijlstra.PrositeScanner.servlets;

import nl.bioinf.hzijlstra.PrositeScanner.model.BrowsingHistory;
import nl.bioinf.hzijlstra.PrositeScanner.model.Prosites;
import nl.bioinf.hzijlstra.PrositeScanner.model.Protein;
import nl.bioinf.hzijlstra.PrositeScanner.config.WebConfig;
import org.thymeleaf.context.WebContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

@WebServlet(name = "PrositeSiteScanner", urlPatterns = {"/"})
public class PrositeScannerServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
        WebContext ctx = new WebContext(request,response,request.getServletContext(), request.getLocale());
        WebConfig.getTemplateEngine().process("prosite-scanner", ctx, response.getWriter());
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
        WebContext ctx = new WebContext(request,response,request.getServletContext(), request.getLocale());
        String proteinSequence = request.getParameter("proteinSequence").toUpperCase();
        List<Prosites> prosites = WebConfig.getPrositePatterns();
        String message = "";


        if (Protein.validateProteinSequence(proteinSequence)) {
            List<Prosites> foundProsites = findProsites(prosites, proteinSequence);
            if (foundProsites.isEmpty()) {
                message = "The sequence you have entered does not contain prosites";
            } else {
                Protein protein = new Protein(proteinSequence, foundProsites);
                addProteinToHistory(request, protein);
                ctx.setVariable("foundProsites", protein);
                message = "The following prosites have been found for sequence: " + protein.getProteinSequence() ;
            }

        } else {
            message = "The sequence you have entered is invalid, make sure your sequence contains valid one letter amino acids";
        }

        ctx.setVariable("message", message);
        WebConfig.getTemplateEngine().process("prosite-scanner", ctx, response.getWriter());
    }

    /**
     * Method that finds matching prosites in a protein sequence
     * returns a list of matching prosites
     * @param prosites
     * @param proteinSequence
     * @return List<Prosites>
     **/
    private List findProsites(List<Prosites> prosites, String proteinSequence) {
        List<Prosites> foundProsites = new ArrayList<>();
        for (int i = 0; i < prosites.size(); i++) {
            Matcher matcher = prosites.get(i).getPattern().matcher(proteinSequence);
            while (matcher.find()) {
                foundProsites.add(prosites.get(i));
            }
        }
        return foundProsites;
    }

    /**
     * Method that adds a queries protein to history
     * @param request
     * @param protein
     **/
    private void addProteinToHistory(HttpServletRequest request, Protein protein) {
        HttpSession session = request.getSession();
        if (session.getAttribute("history") == null) {
            BrowsingHistory browsingHistory = new BrowsingHistory();
            browsingHistory.addItem(protein);
            session.setAttribute("history", browsingHistory );

        } else {
            BrowsingHistory att = (BrowsingHistory)session.getAttribute("history");
            att.addItem(protein);
        }

    }



}