package nl.bioinf.hzijlstra.PrositeScanner.model;

import java.util.Collections;
import java.util.List;

/**
 * Class that represents a protein with none or multiple prosite patterns
 * Able to validate protein sequences on authenticity
 * @author Hans Zijlstra
 *
 */
public class Protein {
    private final String proteinSequence;
    private List<Prosites> prositePatterns;


    public Protein(String proteinSequence, List<Prosites> prositePatterns) {
        this.proteinSequence = proteinSequence;
        this.prositePatterns = prositePatterns;
    }

    public String getProteinSequence() {
        return proteinSequence;
    }

    public List<Prosites> getPrositePatterns() {
        return Collections.unmodifiableList(prositePatterns);
    }

    /**
     *Static method that returns a boolean if a protein sequence is authentic
     * @param sequence protein sequence
     * @return boolean
     */
    public static boolean validateProteinSequence(String sequence) {
        return ((!sequence.equals("")) && (sequence != null) &&
                (sequence.matches("^" + Prosites.getValidAminoAcidsRegexPattern() + "*$")));

    }

    @Override
    public String toString() {
        return "Protein{" +
                "proteinSequence='" + proteinSequence + '\'' +
                ", prositePatterns=" + prositePatterns +
                '}';
    }
}
