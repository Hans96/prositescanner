package nl.bioinf.hzijlstra.PrositeScanner.model;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class that represents prosites within a protein sequence
 * holds information regarding the prosite pattern and options to convert this
 * to regular expression
 * @author Hans Zijlstra
 */


public class Prosites {
    private final String id;
    private final String accessionId;
    private final String[] dates;
    private final String description;
    private final Pattern pattern;
    private final static Pattern negativeLookAhead = Pattern.compile("\\{(\\w)}");
    private final static String validAminoAcidsRegexPattern = "[ARNDCEQGHILKMFPSTWYV]";

    public Prosites(String id, String accessionId, String description, String[] dates, Pattern pattern) {
        this.id = id;
        this.accessionId = accessionId;
        this.description = description;
        this.dates = dates;
        this.pattern = pattern;
    }


    public String getId() {
        return id;
    }

    public Pattern getPattern() {
        return pattern;
    }

    public String[] getDates() {
        return dates;
    }

    public String getAccessionId() {
        return accessionId;
    }

    public String getDescription() {
        return description;
    }



    public static Pattern getNegativeLookAhead() {return negativeLookAhead; }

    public static String getValidAminoAcidsRegexPattern() { return validAminoAcidsRegexPattern;}

    /**
     * Parsing of prosite file contents into a list
     * @param filePath String
     * @return List<String>
     * */
    public static List<String> parsePrositeFile(String filePath) {
        Path path = Paths.get(filePath);
        List<String> prositeLines = new ArrayList<>();

        try (BufferedReader bufferedReader = Files.newBufferedReader(path)) {
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                if (line.startsWith("AC") || line.startsWith("ID") || line.startsWith("DT") || line.startsWith("PA") || line.startsWith("DE")) {
                    prositeLines.add(line);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return prositeLines;
    }

    /**
     * Method that retrieves prosite information and stores this in Prosites object
     * A list of prosites objects is returned
     * @param prositeLines list<String>
     * @return  List<Prosites>
     **/
    public static List<Prosites> getPrositePatternData(List<String> prositeLines) {
        List<Prosites> allPrositePatterns = new ArrayList<>();
        String id = "";
        String ac = "";
        StringBuilder pa = new StringBuilder();
        String[] dt = new String[3];
        String de = "";

        for (int i = 0; i < prositeLines.size(); i++) {
            String line = prositeLines.get(i);
            if(line.endsWith("PATTERN.")) {
                if (!pa.toString().equals("")) {
                    Pattern prositeRegexPattern = convertPrositePatternToRegex(convertNegativeLookahead(pa.toString()));
                    Prosites prosites = new Prosites(id, ac, de, dt, prositeRegexPattern);
                    allPrositePatterns.add(prosites);
                }
                id = line.substring(4).split(";")[0];
                pa = new StringBuilder();
                dt = new String[3];
                de = "";

            } else if (line.startsWith("DT")) {
                dt = line.substring(4).split(";");
            } else if (line.startsWith("DE")) {
                de = line.substring(4);
            } else if (line.startsWith("AC")) {
                ac = line;
            } else {
                pa.append(line.split("\\s+")[1]);
            }
        }
        return allPrositePatterns;
    }
    /**
     * Converts neggative lookahead for prosite patterns
     * to a negative lookahead pattern for regex
     * @param prositePattern String
     * @return String
     * **/
    private static String convertNegativeLookahead(String prositePattern) {
        Matcher matcher = negativeLookAhead.matcher(prositePattern);
        HashSet<Integer> keySet = new HashSet<>();

        while (matcher.find()) {
            keySet.add(matcher.start() + 1);
        }
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < prositePattern.length(); i++) {
            if (keySet.contains(i)) {
                result.append("^");
            }
            result.append(prositePattern.charAt(i));
        }
        return result.toString();
    }

    /**
     * Converts a prosite pattern to regex pattern
     * @param pa String
     * @return Pattern
     **/
    private static Pattern convertPrositePatternToRegex(String pa) {
        String prositePattern = pa.replaceAll("-", "").replaceAll("x", validAminoAcidsRegexPattern)
                .replaceAll("\\{", "[").replaceAll("}", "]").replaceAll("\\(", "{").replaceAll("\\)", "}")
                .replace(".", "");
        return Pattern.compile(prositePattern);

    }

}