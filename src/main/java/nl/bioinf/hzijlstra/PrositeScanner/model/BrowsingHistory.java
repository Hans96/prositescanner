package nl.bioinf.hzijlstra.PrositeScanner.model;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Class that holds the history of queried protein classes
 * @author Hans Zijlstra
 */
public class BrowsingHistory {
    private static final int DEFAULT_MAXIMUM_SIZE = 5;
    private int maximumSize;
    private LinkedList<Protein> history = new LinkedList<>();

    public BrowsingHistory() {
        this.maximumSize = DEFAULT_MAXIMUM_SIZE;
    }

    public BrowsingHistory(int maximumSize) {
        this.maximumSize = maximumSize;
    }


    /**
     * Method that adds protein to history list
     **/
    public void addItem(Protein historyItem) {
        if (history.size() == maximumSize) {
            history.remove(history.size() - 1);
        }
        history.add(0, historyItem);
    }

    public List<Protein> getHistoryItems() {

        return Collections.unmodifiableList(this.history);
    }

    @Override
    public String toString() {
        return "BrowsingHistory{" +
                "history=" + history.toString() +
                '}';
    }
}
