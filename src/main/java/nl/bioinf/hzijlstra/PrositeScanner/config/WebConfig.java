package nl.bioinf.hzijlstra.PrositeScanner.config;

import nl.bioinf.hzijlstra.PrositeScanner.model.Prosites;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;

/**
 * Configuration class for rendering PrositeScanner application
 **/
@WebListener
public class WebConfig implements ServletContextListener {
    private static TemplateEngine templateEngine;
    private static List<Prosites> prositePaterns;

    /**
     * Creates a template engine for rendering pages
     * @param servletContext
    **/
    private static void createTemplateEngine(ServletContext servletContext) {
        ServletContextTemplateResolver templateResolver =
                new ServletContextTemplateResolver(servletContext);
        templateResolver.setTemplateMode("XHTML");
        templateResolver.setPrefix("/WEB-INF/templates/");
        templateResolver.setSuffix(".html");
        templateResolver.setCacheTTLMs(3600000L);
        // Cache is set to true by default.
        // Set to false if you want templates to be automatically
        // updated when modified.
        templateResolver.setCacheable(true);

        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);
        WebConfig.templateEngine = templateEngine;
    }
    /**
     * Configures the response in a standard way.
     * @param response
     */
    public static void configureResponse(HttpServletResponse response) {
        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        String datFile = sce.getServletContext().getInitParameter("dateFile");
        WebConfig.prositePaterns = Prosites.getPrositePatternData(Prosites.parsePrositeFile(datFile));
        createTemplateEngine(sce.getServletContext());

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("Shutting down!");

    }
    public static TemplateEngine getTemplateEngine() {
        return templateEngine;
    }

    public static List getPrositePatterns() {
        return Collections.unmodifiableList(prositePaterns);
    }
}