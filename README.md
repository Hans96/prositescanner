# PrositeScanner#

This application scans a protein sequence for known prosites and holds a history of found prosites. Information regarding the prosite is displayed by means of a table.  


### How do I get set up? ###

This application is build under java 11 and Apache Tomcat 9.0.34 is used to serve and render webpages by making use of servlets. For compatability reasons it is adviced to have java 11 installed on your working station

Furthermore, the web.xml in this repo contains a path to a prosite.dat file used to parse prosite data.
Therefore these paths need to be adjusted to suit your own working environment. 
For example:

```java

<context-param>
        <param-name>dateFile</param-name>
        <param-value>C:\Users\Hans\Desktop\backupprosite\PrositeScanner\data\prosite.dat</param-value>
    </context-param>
```

Apache Tomcat 9.0.34 is used to serve and render webpages by making use of servlets. 



### Contact ###
For help or more information please contact Hans Zijlstra at:

* ha.zijlstra@st.hanze.nl
